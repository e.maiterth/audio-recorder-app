import 'package:audio_recorder_app/audio_visualizer.dart';
import 'package:flutter/material.dart';

class AudioVisualizationScreen extends StatefulWidget {
  const AudioVisualizationScreen(
      {super.key, required this.magnitudes, required this.frequencies});

  final List<double> magnitudes;
  final List<double> frequencies;

  @override
  AudioVisualizationScreenState createState() =>
      AudioVisualizationScreenState();
}

class AudioVisualizationScreenState extends State<AudioVisualizationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Frequency Spectrum'),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: AudioVisualizer(
            magnitudes: widget.magnitudes,
            frequencies: widget.frequencies,
          ),
        ),
      ),
    );
  }
}
