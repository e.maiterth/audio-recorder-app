import 'package:audio_recorder_app/audio_recorder_view.dart';
import 'package:audio_recorder_app/recorder_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RecorderScreen extends StatelessWidget {
  const RecorderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => RecorderProvider()),
      ],
      child: const RecorderView(),
    );
  }
}
