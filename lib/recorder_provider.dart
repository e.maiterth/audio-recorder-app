import 'package:flutter/material.dart';

class RecorderProvider extends ChangeNotifier {
  int _sampleRate = 44100;
  int _bitRate = 16000;
  int _numChannels = 2;

  int get sampleRate => _sampleRate;
  int get bitRate => _bitRate;
  int get numChannels => _numChannels;

  void setSampleRate(int value) {
    _sampleRate = value;
    notifyListeners();
  }

  void setBitRate(int value) {
    _bitRate = value;
    notifyListeners();
  }

  void setNumChannels(int value) {
    _numChannels = value;
    notifyListeners();
  }
}
