import 'dart:io';

import 'package:audio_recorder_app/info_screen.dart';
import 'package:audio_recorder_app/recorder_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_sound/public/flutter_sound_recorder.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class RecorderView extends StatefulWidget {
  const RecorderView({super.key});

  @override
  RecorderViewState createState() => RecorderViewState();
}

class RecorderViewState extends State<RecorderView> {
  FlutterSoundRecorder recorder = FlutterSoundRecorder();
  bool isRecorderReady = false;

  @override
  void initState() {
    super.initState();

    initRecorder();
  }

  @override
  void dispose() {
    recorder.closeRecorder();

    super.dispose();
  }

  Future record(
    int sampleRate,
    int bitRate,
    int numChannels,
  ) async {
    if (!isRecorderReady) return;

    await recorder.startRecorder(
      toFile: "audio",
      sampleRate: sampleRate,
      bitRate: bitRate,
      numChannels: numChannels,
    );

    setState(() {});
  }

  Future stop() async {
    if (!isRecorderReady) return;

    final path = await recorder.stopRecorder();
    final audioFile = File(path!);

    print("Recorded audio: $audioFile");
  }

  @override
  Widget build(BuildContext context) {
    final RecorderProvider recorderProvider =
        Provider.of<RecorderProvider>(context);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            StreamBuilder<RecordingDisposition>(
              stream: recorder.onProgress,
              builder: (context, snapshot) {
                final duration =
                    snapshot.hasData ? snapshot.data!.duration : Duration.zero;

                String twoDigits(int n) => n.toString().padLeft(2, '0');
                final twoDigitsMinutes =
                    twoDigits(duration.inMinutes.remainder(60));
                final twoDigitsSeconds =
                    twoDigits(duration.inSeconds.remainder(60));

                return Text(
                  "$twoDigitsMinutes:$twoDigitsSeconds",
                  style: const TextStyle(
                      fontSize: 80, fontWeight: FontWeight.bold),
                );
              },
            ),
            const SizedBox(height: 32),
            ElevatedButton(
              child: Icon(
                recorder.isRecording ? Icons.stop : Icons.mic,
                size: 80,
              ),
              onPressed: () async {
                if (recorder.isRecording) {
                  await stop();
                } else {
                  await record(
                    recorderProvider.sampleRate,
                    recorderProvider.bitRate,
                    recorderProvider.numChannels,
                  );
                }

                setState(() {});
              },
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  FormBuilderDropdown(
                    name: "num_channels",
                    decoration: const InputDecoration(
                      labelText: "Number of Channels",
                      border: InputBorder.none,
                      helperText: "Mono or Stereo.",
                    ),
                    initialValue: recorderProvider.numChannels,
                    items: [1, 2]
                        .map(
                          (e) => DropdownMenuItem(
                            value: e,
                            child: Text("$e Channel${e == 1 ? "" : "s"}"),
                          ),
                        )
                        .toList(),
                  ),
                  FormBuilderDropdown(
                    name: "sample_rate",
                    items: [
                      8000,
                      16000,
                      32000,
                      44100,
                      48000,
                      96000,
                      192000,
                      384000,
                      512000
                    ]
                        .map((rate) => DropdownMenuItem(
                              value: rate,
                              child: Text("${rate / 1000} kHz"),
                            ))
                        .toList(),
                    decoration: const InputDecoration(
                      labelText: "Sample Rate",
                      border: InputBorder.none,
                    ),
                    onChanged: (value) {
                      recorderProvider.setSampleRate(value as int);
                    },
                    initialValue: recorderProvider.sampleRate,
                  ),

                  /// Bitrate is in bits per second.
                  FormBuilderDropdown(
                    name: "bit_rate",
                    items: [
                      8000,
                      16000,
                      32000,
                      44100,
                      48000,
                      96000,
                      192000,
                      384000,
                      512000
                    ]
                        .map((rate) => DropdownMenuItem(
                              value: rate,
                              child: Text("${rate / 1000} kbps"),
                            ))
                        .toList(),
                    decoration: const InputDecoration(
                      labelText: "Bit Rate",
                      border: InputBorder.none,
                    ),
                    onChanged: (value) {
                      recorderProvider.setBitRate(value as int);
                    },
                    initialValue: recorderProvider.bitRate,
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.zero,
                    title: const Text("Information"),
                    subtitle: const Text(
                        "Tap here to learn more about audio recording."),
                    trailing: const Icon(Icons.arrow_forward_ios),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const InfoScreen(),
                        ),
                      );
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future initRecorder() async {
    final status = await Permission.microphone.request();
    if (status != PermissionStatus.granted) {
      throw "Microphone permission not granted";
    }

    await recorder.openRecorder();

    isRecorderReady = true;
    await recorder.setSubscriptionDuration(
      const Duration(milliseconds: 50),
    );
  }
}
