import 'package:flutter/material.dart';

class InfoScreen extends StatelessWidget {
  const InfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Scrollbar(
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Audio Recording Basics",
                      style: Theme.of(context).textTheme.headlineMedium),
                  const SizedBox(height: 16),
                  Text(
                    "This app demonstrates how to record audio using the microphone on your device. The app uses the flutter_sound package to record audio and save it to a file. The app also demonstrates how to play the recorded audio file.",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 16),
                  Text(
                    "To record audio, tap the microphone icon. To stop recording, tap the stop icon. To play the recorded audio, switch to the Analyze tab and tap the play icon.",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),

                  const SizedBox(height: 16),

                  /// Details about recording parameters
                  /// Sample rate, bit rate, and number of channels
                  Text(
                    "Recording Parameters",
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  const SizedBox(height: 16),
                  Text(
                    "You can configure the sample rate, bit rate, and number of channels for recording audio. Following section explain the effect of these parameters on audio recording.",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 16),
                  Text(
                    "Sample Rate",
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                  const SizedBox(height: 8),
                  Text(
                    "The sample rate is the number of samples of audio carried per second, measured in Hz or kHz. The sample rate determines the frequency range of the audio signal. The higher the sample rate, the better the audio quality.",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 16),
                  Text(
                    "Bit Rate",
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                  const SizedBox(height: 8),
                  Text(
                    "The bit rate is the number of bits that are processed per unit of time. The bit rate determines the audio quality and file size. The higher the bit rate, the better the audio quality.",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 16),
                  Text(
                    "Number of Channels",
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                  const SizedBox(height: 8),
                  Text(
                    "The number of channels determines the number of audio signals that are recorded. Mono audio has one channel, while stereo audio has two channels. Stereo audio provides a richer audio experience.",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  // CTA to record audio
                  const SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size.fromHeight(44)),
                    child: const Text(
                      "Try it out!",
                    ),
                  ),
                  const SizedBox(height: 16),
                ],
              ),
            ),
          ),
        ));
  }
}
