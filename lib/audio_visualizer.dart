import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class AudioVisualizer extends StatelessWidget {
  final List<double> magnitudes;
  final List<double> frequencies;

  const AudioVisualizer(
      {super.key, required this.magnitudes, required this.frequencies});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: LineChart(
            LineChartData(
              gridData: FlGridData(show: true),
              titlesData: FlTitlesData(
                bottomTitles: SideTitles(
                  interval: 10000,
                  showTitles: true,
                  getTitles: (value) {
                    return '${value.toInt() / 1000} kHz';
                  },
                ),
                leftTitles: SideTitles(
                  showTitles: true,
                  interval: 10,
                  getTitles: (value) {
                    return '${value.toInt()} dB';
                  },
                  
                ),
              ),
              borderData: FlBorderData(
                show: true,
                border: Border.all(color: const Color(0xff37434d)),
              ),
              minX: frequencies.first,
              maxX: frequencies.last,
              minY: 0,
              maxY: magnitudes.reduce((a, b) => a > b ? a : b),
              lineBarsData: [
                LineChartBarData(
                  spots: _createLineSpots(),
                  isCurved: false,
                  colors: [Theme.of(context).primaryColor],
                  barWidth: 2,
                  belowBarData: BarAreaData(show: false),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  List<FlSpot> _createLineSpots() {
    final spots = <FlSpot>[];
    for (var i = 0; i < magnitudes.length; i++) {
      spots.add(
        FlSpot(
          frequencies[i],
          magnitudes[i],
        ),
      );
    }
    return spots;
  }
}
