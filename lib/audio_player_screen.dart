import 'package:audio_recorder_app/analyze_audio.dart';
import 'package:audio_recorder_app/audio_visualization_screen.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class AudioPlayerScreen extends StatefulWidget {
  const AudioPlayerScreen({super.key});

  @override
  AudioPlayerScreenState createState() => AudioPlayerScreenState();
}

class AudioPlayerScreenState extends State<AudioPlayerScreen> {
  final audioPlayer = AudioPlayer();

  bool isPlaying = false;
  Duration duration = Duration.zero;
  Duration position = Duration.zero;

  bool isAudioSet = false;

  final List<Color> colors = [
    Colors.red[900]!,
    Colors.green[900]!,
    Colors.blue[900]!,
    Colors.brown[900]!
  ];

  final List<int> waveDuration = [900, 700, 600, 800, 500];

  @override
  void dispose() {
    audioPlayer.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    audioPlayer.onPlayerStateChanged.listen(onPlayerStateChanged);

    audioPlayer.onDurationChanged.listen(onDurationChanged);

    audioPlayer.onPositionChanged.listen(onPositionChanged);
  }

  void onPlayerStateChanged(PlayerState state) {
    setState(() {
      isPlaying = state == PlayerState.playing;
    });
  }

  /// called when the position of the audio player changes
  void onPositionChanged(Duration position) {
    setState(() {
      this.position = position;
    });
  }

  void onDurationChanged(Duration duration) {
    setState(() {
      this.duration = duration;
    });
  }

  Future setAudio() async {
    /// Load from file

    await audioPlayer.setSourceDeviceFile(
      '/data/user/0/com.example.audio_recorder_app/cache/audio',
    );

    isAudioSet = true;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final ScaffoldMessengerState messenger = ScaffoldMessenger.of(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Text("Example Audio Visualization",
            //     style: Theme.of(context).textTheme.titleMedium),
            // const AudioVisualization(),
            // const SizedBox(height: 32),
            const SizedBox(height: 4),
            Text(
              "Record Audio to analyze it.",
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            Slider(
              min: 0,
              max: duration.inMilliseconds.toDouble(),
              value: position.inMilliseconds.toDouble(),
              onChanged: (value) async {
                final position = Duration(milliseconds: value.toInt());
                await audioPlayer.seek(position);
                await audioPlayer.resume();
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(formatTime(position)),
                Text(formatTime(duration - position)),
              ],
            ),
            CircleAvatar(
              radius: 35,
              child: IconButton(
                icon: Icon(isPlaying ? Icons.pause : Icons.play_arrow),
                onPressed: () async {
                  if (isPlaying) {
                    await audioPlayer.pause();
                  } else {
                    try {
                      await setAudio();
                    } catch (e) {
                      messenger.showSnackBar(
                        SnackBar(
                          content: Text(
                              "Error: $e No audio source found. Please record audio first."),
                        ),
                      );
                      return;
                    }
                    if (audioPlayer.source == null) {
                      messenger.showSnackBar(
                        const SnackBar(
                          content: Text(
                              "No audio source found. Please record audio first."),
                        ),
                      );
                      return;
                    }

                    await audioPlayer.play(audioPlayer.source!);
                  }
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            if (isAudioSet)
              ListTile(
                title: const Text("View Audio Visualization"),
                leading: const Icon(Icons.analytics),
                onTap: () async {
                  final timeDomainData = await readAudioFile(
                      "/data/user/0/com.example.audio_recorder_app/cache/audio");

                  final fftResult = performFFT(timeDomainData);

                  final magnitudes = fftResult[0];
                  final frequencies = fftResult[1];

                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => AudioVisualizationScreen(
                              magnitudes: magnitudes,
                              frequencies: frequencies,
                            )),
                  );
                },
              ),
          ],
        ),
      ),
    );
  }

  String formatTime(Duration duration) {
    return duration.toString().split('.').first.padLeft(8, '0');
  }
}
