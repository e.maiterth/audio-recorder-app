import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:fftea/impl.dart';
import 'package:fftea/stft.dart';
import 'package:fftea/util.dart';

import 'package:flutter_sound/public/util/flutter_sound_helper.dart';

Future<List<double>> readAudioFile(String filePath) async {
  final file = File(filePath);
  final bytes = await file.readAsBytes();
  final sound = FlutterSoundHelper();

  // Decode audio file to PCM format
  final pcmData = sound.waveToPCMBuffer(inputBuffer: bytes);

  // Convert Int16List to List<double>
  return pcmData.map((e) => e.toDouble()).toList();
}

List<List<double>> performFFT(List<double> samples) {
  const chunkSize = 1234;
  final stft = STFT(chunkSize, Window.hanning(chunkSize));

  // Compute the STFT and generate the spectrogram
  final spectrogram = <Float64List>[];
  stft.run(samples, (Float64x2List freq) {
    spectrogram.add(freq.discardConjugates().magnitudes());
  });

  List<double> magnitudes = [];

  if (spectrogram.isNotEmpty) {
    magnitudes =
        spectrogram[0].map((m) => 20 * log(m) / ln10).toList(); // Convert to dB
  }

  // Calculate the frequencies for each bin in the spectrogram
  const sampleRate = 44100;
  final binCount = magnitudes.length;
  List<double> frequencies = List<double>.generate(
      binCount, (i) => stft.frequency(i, sampleRate.toDouble()));

  // Remove the DC component (0 Hz)
  if (frequencies.isNotEmpty && magnitudes.isNotEmpty) {
    frequencies = frequencies.sublist(1);
    magnitudes = magnitudes.sublist(1);
  }

  // Output the frequencies
  for (var i = 0; i < frequencies.length; i++) {
    print('Frequency bin $i: ${frequencies[i]} Hz');
  }

  // Convert Float64x2List to List<double>
  return [magnitudes, frequencies];
}

List<double> getFrequencies(int fftSize, int sampleRate) {
  return List<double>.generate(fftSize ~/ 2, (i) => i * sampleRate / fftSize);
}
