# audio_recorder_app

This is an audio recorder app that records audio and saves it to the device storage. The app is built using Flutter and Dart.

## Framework

- Flutter is a UI toolkit that enables the development of natively compiled applications for mobile, web, and desktop from a single codebase.
- Dart is a client-optimized programming language for fast apps on any platform.

## Features

- Record audio, play and pause audio
- Save audio to device storage
- Adjust recording parameters
- Visualize the frequency domain of the recorded audio


## Dependencies

- `audioplayers` package to record and play audio
- `path_provider` package to access and get the path of the device storage.
- `permission_handler`for requesting and checking the microphone permission state
- `fftea` for performing the Fast Fourier Transformation (FFT)
- `fl_chart`for visualising the output of the FFT

## Improvement Possibilities

- Also see the time domain
- Apply filters, f.e. a low- or high-pass filter
- Raise alarms (send notifications) when frequency falls out of specified bounds (application: warning system for industry machines)